import React from 'react';
import { Text, View, TextInput, Button, TouchableOpacity } from 'react-native';

class App extends React.Component {

  state = {
    text: "",
    todos: []
  }

  addTodo = () => {
    let newTodo = this.state.text;
    let todoArr = this.state.todos;
    todoArr.push(newTodo);
    this.setState({ todos: todoArr, text: "" });
  }

  deleteTodo = (todo) => {
    let _todos = this.state.todos;
    let pos = _todos.indexOf(todo);
    _todos.splice(pos, 1);
    this.setState({ todos: _todos });
  }

  renderTodos = () => {
    return this.state.todos.map(todo => {
      return (
        <TouchableOpacity key={todo}>
          <Text styles={styles.listItems} onPress={() => { this.deleteTodo(todo) }}>{todo}</Text>
        </TouchableOpacity>
      )
    })
  }

  render() {
    return (
      <View style={styles.outer}>
        <View style={styles.viewStyles}>
          <Text style={styles.header}>My Todo List</Text>
          <TextInput
            style={styles.inputStyle}
            onChangeText={(text) => this.setState({ text })}
            value={this.state.text}
          />
          <Button
            title="Add Todo"
            onPress={this.addTodo}
          />

          <View style={{ marginTop: 50 }} />
          {this.renderTodos()}

        </View>
      </View>
    );
  }
}

const styles = {
  outer: {
    flex: 1,
    backgroundColor: '#B2DFDB'
  },
  viewStyles: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10
  },
  inputStyle: {
    height: 40,
    width: 300,
    borderColor: "grey",
    borderWidth: 1,
    padding: 5,
    margin: 20
  },
  header: {
    fontSize: 30,
    fontWeight: "bold",
    margin: 20
  },
  listItems: {
    fontSize: 25,
    padding: 10
  }
}

export default App;

